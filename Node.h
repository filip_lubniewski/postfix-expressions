//
//  Node.h
//  
//
//  Created by macbook pro on 31.10.2016.
//
//

#ifndef ____Node__
#define ____Node__

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include "Utils.h"


using namespace std;

class Node
{
public:
    Node(string value);
    double evaluate();
    friend class ExpressionTree;
    
private:
    Node *mLeftChild, *mRightChild;
    string mValue;
    double mRealValue;
    int levelOfTheNodeInTree;
};

#endif /* defined(____Node__) */
