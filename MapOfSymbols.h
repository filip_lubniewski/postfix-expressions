//
//  MapOfSymbols.h
//  
//
//  Created by macbook pro on 01.11.2016.
//
//

#ifndef ____MapOfSymbols__
#define ____MapOfSymbols__

#include <stdio.h>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <string>
#include "Utils.h"

using namespace std;

class MapOfSymbols
{
public:
    MapOfSymbols(vector<string>* expression);
    void assignValuesToVariables();
    int getValueOfVariable(string variable);
    
private:
    map<string,double> mapOfVariables;
    set<string> variables;
};
#endif /* defined(____MapOfSymbols__) */
