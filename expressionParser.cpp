//
//  ExpressionParser.cpp
//  
//
//  Created by macbook pro on 31.10.2016.
//
//

#include "ExpressionParser.h"
#include <string>
#include <vector>
#include "Utils.h"

ExpressionParser::ExpressionParser(string unprocessedExpression)
{
    mExpression = unprocessedExpression;
}

void ExpressionParser::addSpacesToExpression()
{
    string expressionWithAdditionalSpaces = "";
    
    for(int i = 0; i < mExpression.length(); i++)
    {
        if(Utils::isOperator(mExpression.at(i)))
        {
            string operatorWithSpaces = " ";
            operatorWithSpaces += mExpression.at(i);
            operatorWithSpaces += " ";
            
            expressionWithAdditionalSpaces += operatorWithSpaces;
        }
        else if(Utils::isNumber(mExpression.at(i)) && i+1 < mExpression.length() && Utils::isVariable(mExpression.at(i+1)))
        {
            string numberWithSpace;
            numberWithSpace += mExpression.at(i);
            numberWithSpace += " ";
                                    
            expressionWithAdditionalSpaces += numberWithSpace;
        }
        else
        {
            expressionWithAdditionalSpaces += mExpression.at(i);
        }
    }
    
    mExpression = expressionWithAdditionalSpaces;
}

vector<string>* ExpressionParser::splitExpression()
{
    vector<string> *elementsOfExpression = new vector<string>;
    int startIndexOfElement, endIndexOfElement;
    
    startIndexOfElement = endIndexOfElement = 0;
    addSpacesToExpression();
    
    while(startIndexOfElement < mExpression.length())
    {
        if(mExpression.at(startIndexOfElement) != ' ')
        {
            endIndexOfElement = startIndexOfElement;
            
            while(endIndexOfElement < mExpression.size() && mExpression.at(endIndexOfElement) != ' ')
            {
                endIndexOfElement++;
            }
            
            int lengthOfElement = endIndexOfElement - startIndexOfElement;
                        
            string element = mExpression.substr(startIndexOfElement, lengthOfElement);
            elementsOfExpression->push_back(element);
            
            startIndexOfElement = endIndexOfElement;

        }
        else startIndexOfElement++;
    }
    
    return elementsOfExpression;
}