//
//  ExpressionTree.h
//  
//
//  Created by macbook pro on 31.10.2016.
//
//

#ifndef ____ExpressionTree__
#define ____ExpressionTree__

#include <stdio.h>
#include "Node.h"
#include <string>
#include <vector>
#include <stack>
#include <queue>
#include "Utils.h" 
#include "Operations.h"
#include "MapOfSymbols.h"

const static string TOO_FEW_ARGUMENTS_MSG = "Zabraklo argumentu na pozycji: ";
const static string AUTO_CORRECT_MSG = "za argument zostala podstawiona domyslna wartosc 1";

class ExpressionTree
{
public:
    ExpressionTree(vector<string> *preprocessedExpression, MapOfSymbols *valuesOfVariables);
    Node* buildTree();
    double evaluate();
    void printInfixNotation();
    void printPossibleMistakesInExpression();
    void printTree();
    
private:
    vector<string> *mExpression;
    Node *mRoot;
    MapOfSymbols *mValuesOfVariables;
    vector<string> mMistakesInExpression;
    double evaluateTree(Node *node);
    void getInfixNotation(Node *node);
    void calculateLevelsOfNodes(Node *node);
    void printLevelByLevel(Node *node);
};

#endif /* defined(____ExpressionTree__) */

