//
//  Node.cpp
//  
//
//  Created by macbook pro on 31.10.2016.
//
//

#include "Node.h"

Node::Node(string value)
{
    mValue = value;
    int integerValue;
    
    if(Utils::isNumber(value))
    {
        istringstream iss(value);
        iss >> integerValue;
        mRealValue = integerValue * 1.0;
    }
}

double Node::evaluate()
{
    return mRealValue;
}