//
//  Operations.cpp
//  
//
//  Created by macbook pro on 31.10.2016.
//
//

#include <stdio.h>
#include "Operations.h"

const char Operations::ADDITION_CHAR = '+';
const char Operations::SUBSTRACTION_CHAR = '-';
const char Operations::MULTIPLICATION_CHAR = '*';
const char Operations::DIVISION_CHAR = '/';
const char Operations::OPPOSITION_CHAR = '~';

const string Operations::DEFAULT_VALUE = "1";

