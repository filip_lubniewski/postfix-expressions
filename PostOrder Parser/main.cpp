//
//  main.cpp
//  PostOrder Parser
//
//  Created by macbook pro on 30.10.2016.
//  Copyright (c) 2016 macbook pro. All rights reserved.
//

#include <iostream>
#include <vector>
#include <map>
#include "Node.h"
#include "ExpressionParser.h"
#include "ExpressionTree.h"
#include "MapOfSymbols.h"

using namespace std;

int main(int argc, const char * argv[])
{
    string expression;
    cout << "Podaj wyrazenie w postaci postfiksowej: ";
    getline(cin, expression);
        
    ExpressionParser exp(expression);
    
    vector<string> *splittedExpression = exp.splitExpression();
    
    MapOfSymbols *valuesOfVariables = new MapOfSymbols(splittedExpression);
    valuesOfVariables->assignValuesToVariables();
    
    ExpressionTree tree(splittedExpression, valuesOfVariables);
    tree.buildTree();
    cout << tree.evaluate() << endl;
    
    tree.printPossibleMistakesInExpression();
    tree.printInfixNotation();
    tree.printTree();
    
    return 0;
}
