//
//  Utils.cpp
//  
//
//  Created by macbook pro on 01.11.2016.
//
//

#include "Utils.h"

bool Utils::isOperator(string sign)
{
    char firstSign = sign[0];
    
    bool isOperator =
    firstSign == Operations::ADDITION_CHAR ||
    firstSign == Operations::SUBSTRACTION_CHAR ||
    firstSign == Operations::MULTIPLICATION_CHAR ||
    firstSign == Operations::DIVISION_CHAR ||
    firstSign == Operations::OPPOSITION_CHAR;
    
    return isOperator;
}

bool Utils::isOperator(char sign)
{
    bool isOperator =
    sign == Operations::ADDITION_CHAR ||
    sign == Operations::SUBSTRACTION_CHAR ||
    sign == Operations::MULTIPLICATION_CHAR ||
    sign == Operations::DIVISION_CHAR ||
    sign == Operations::OPPOSITION_CHAR;
    
    return isOperator;
}

bool Utils::isBinaryOperator(string sign)
{
    char firstSign = sign[0];
    
    bool isOperator =
    firstSign == Operations::ADDITION_CHAR ||
    firstSign == Operations::SUBSTRACTION_CHAR ||
    firstSign == Operations::MULTIPLICATION_CHAR ||
    firstSign == Operations::DIVISION_CHAR;
    
    return isOperator;
}

bool Utils::isUnaryOperator(string sign)
{
    char firstSign = sign[0];
    
    bool isOperator = firstSign == Operations::OPPOSITION_CHAR;
    
    return isOperator;
}

bool Utils::isVariable(char value)
{
    return (value > 64 && value < 91) || (value > 96 && value < 123);
}

bool Utils::isVariable(string value)
{
    return (value[0] > 64 && value[0] < 91) || (value[0] > 96 && value[0] < 123);
}

bool Utils::isNumber(char value)
{
    return value > 47 && value < 58;
}

bool Utils::isNumber(string value)
{
    return value[0] > 47 && value[0] < 58;
}