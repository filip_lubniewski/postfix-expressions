//
//  MapOfSymbols.cpp
//  
//
//  Created by macbook pro on 01.11.2016.
//
//

#include "MapOfSymbols.h"

MapOfSymbols::MapOfSymbols(vector<string>* expression)
{
    for(int i = 0; i < expression->size(); i++)
    {
        if(Utils::isVariable(expression->at(i)))
        {
            variables.insert(expression->at(i));
        }
    }
}

void MapOfSymbols::assignValuesToVariables()
{
    set<string>::iterator it;
    double value;
    
    for(it = variables.begin(); it != variables.end(); ++it)
    {
        cout << "Podaj wartosc dla " << *it << ": ";
        cin >> value;
        mapOfVariables[*it] = value;
    }
}

int MapOfSymbols::getValueOfVariable(string variable)
{
    return mapOfVariables[variable];
}