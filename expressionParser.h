//
//  ExpressionParser.h
//  
//
//  Created by macbook pro on 31.10.2016.
//
//

#ifndef ____ExpressionParser__
#define ____ExpressionParser__

#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

class ExpressionParser
{
public:
    ExpressionParser(string unprocessedExpression);
    vector<string> *splitExpression();
    
private:
    string mExpression;
    void addSpacesToExpression();
    
};

#endif /* defined(____ExpressionParser__) */
