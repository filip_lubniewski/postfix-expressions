//
//  Utils.h
//  
//
//  Created by macbook pro on 01.11.2016.
//
//

#ifndef ____Utils__
#define ____Utils__

#include <stdio.h>
#include <string>
#include "Operations.h"

using namespace std;

class Utils
{
public:
    static bool isOperator(string sign);
    static bool isOperator(char sign);
    static bool isUnaryOperator(string sign);
    static bool isBinaryOperator(string sign);
    static bool isVariable(string value);
    static bool isVariable(char value);
    static bool isNumber(char value);
    static bool isNumber(string value);
};
#endif /* defined(____Utils__) */
