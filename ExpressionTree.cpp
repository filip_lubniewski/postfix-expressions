//
//  ExpressionTree.cpp
//  
//
//  Created by macbook pro on 31.10.2016.
//
//

#include "ExpressionTree.h"


ExpressionTree::ExpressionTree(vector<string> *preprocessedExpression, MapOfSymbols *valuesOfVariables)
{
    mExpression = preprocessedExpression;
    mValuesOfVariables = valuesOfVariables;
}

Node* ExpressionTree::buildTree()
{
    stack<Node*> *stackOfNodes = new stack<Node*>;
    
    for(int i = 0; i < mExpression->size(); i++)
    {
        string element = mExpression->at(i);
        Node *newNode = new Node(element);
        
        Node *firstElement, *secondElement;
        
        if(Utils::isVariable(element) || Utils::isNumber(element))
        {
            stackOfNodes->push(newNode);
        }
        else if(Utils::isUnaryOperator(element))
        {
            if(!stackOfNodes->empty())
            {
                firstElement = stackOfNodes->top();
                stackOfNodes->pop();
                
                newNode->mLeftChild = firstElement;
            }
            else
            {
                firstElement = new Node(Operations::DEFAULT_VALUE);
                newNode->mLeftChild = firstElement;
                
                mMistakesInExpression.push_back(TOO_FEW_ARGUMENTS_MSG + to_string(i) + "\n" + AUTO_CORRECT_MSG);
            }
            
            stackOfNodes->push(newNode);
        }
        else
        {
            if(!stackOfNodes->empty())
            {
                firstElement = stackOfNodes->top();
                stackOfNodes->pop();
                
                newNode->mRightChild = firstElement;
            }
            else
            {
                firstElement = new Node(Operations::DEFAULT_VALUE);
                newNode->mRightChild = firstElement;
                
                mMistakesInExpression.push_back(TOO_FEW_ARGUMENTS_MSG + to_string(i) + "\n" + AUTO_CORRECT_MSG);
            }
            
            if(!stackOfNodes->empty())
            {
                secondElement = stackOfNodes->top();
                stackOfNodes->pop();
                
                newNode->mLeftChild = secondElement;
            }
            else
            {
                firstElement = new Node(Operations::DEFAULT_VALUE);
                newNode->mLeftChild = firstElement;
                
                mMistakesInExpression.push_back(TOO_FEW_ARGUMENTS_MSG + to_string(i) + "\n" + AUTO_CORRECT_MSG);
            }
            
            stackOfNodes->push(newNode);
        }
        
    }
    
    mRoot = stackOfNodes->top();
    return stackOfNodes->top();
}

double ExpressionTree::evaluate()
{
    return evaluateTree(mRoot);
}

double ExpressionTree::evaluateTree(Node *node)
{
    if(node->mRightChild == NULL && node->mLeftChild == NULL)
    {
        if(Utils::isVariable(node->mValue))
        {
            return mValuesOfVariables->getValueOfVariable(node->mValue);
        }
        else
        {
            return node->evaluate();
        }
    }
    else
    {
        if(Operations::OPPOSITION_CHAR == node->mValue[0])
        {
            return evaluateTree(node->mLeftChild) * -1;
        }
        else if(Operations::ADDITION_CHAR == node->mValue[0])
        {
            return evaluateTree(node->mLeftChild) + evaluateTree(node->mRightChild);
        }
        else if(Operations::SUBSTRACTION_CHAR == node->mValue[0])
        {
            return evaluateTree(node->mLeftChild) - evaluateTree(node->mRightChild);
        }
        else if(Operations::DIVISION_CHAR == node->mValue[0])
        {
            return evaluateTree(node->mLeftChild) / evaluateTree(node->mRightChild);
        }
        else
        {
            return evaluateTree(node->mLeftChild) * evaluateTree(node->mRightChild);
        }
    }
}

void ExpressionTree::printInfixNotation()
{
    getInfixNotation(mRoot);
}

void ExpressionTree::getInfixNotation(Node *node)
{
    if(!(node->mLeftChild == NULL && node->mRightChild == NULL)) cout << "(";
    
    if(node->mLeftChild != NULL) getInfixNotation(node->mLeftChild);
    
    cout << node->mValue;
    
    if(node->mRightChild != NULL) getInfixNotation(node->mRightChild);
    
    if(!(node->mLeftChild == NULL && node->mRightChild == NULL)) cout << ")";
}

void ExpressionTree::printPossibleMistakesInExpression()
{
    for(int i = 0; i < mMistakesInExpression.size(); i++)
    {
        cout << mMistakesInExpression.at(i) << endl;
    }
}

void ExpressionTree::calculateLevelsOfNodes(Node *node)
{
    if (node == NULL)  return;
    
    queue<Node*> queue;
    queue.push(node);
    
    node->levelOfTheNodeInTree = 0;
    
    while (!queue.empty())
    {
        Node *node = queue.front();
        queue.pop();
        
        if (node->mLeftChild != NULL)
        {
            node->mLeftChild->levelOfTheNodeInTree = node->levelOfTheNodeInTree + 1;
            queue.push(node->mLeftChild);
        }
        
        
        if (node->mRightChild != NULL)
        {
            node->mRightChild->levelOfTheNodeInTree = node->levelOfTheNodeInTree + 1;
            queue.push(node->mRightChild);
        }
    }
}

void ExpressionTree::printTree()
{
    calculateLevelsOfNodes(mRoot);
    printLevelByLevel(mRoot);
}

void ExpressionTree::printLevelByLevel(Node *node)
{    
    if (node == NULL)  return;
    
    queue<Node*> queue;
    queue.push(node);
    
    int lastPrintedLevel = node->levelOfTheNodeInTree - 1;
    
    while (!queue.empty())
    {
        Node *node = queue.front();
        
        if(lastPrintedLevel < node->levelOfTheNodeInTree)
        {
            lastPrintedLevel = node->levelOfTheNodeInTree;
            cout << endl;
        }
        
        cout << node->mValue << " ";
        queue.pop();
        
        
        if (node->mLeftChild != NULL)
            queue.push(node->mLeftChild);
        
        if (node->mRightChild != NULL)
            queue.push(node->mRightChild);
    }
}



