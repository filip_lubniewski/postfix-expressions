//
//  Operations.h
//  PostOrder Parser
//
//  Created by macbook pro on 31.10.2016.
//  Copyright (c) 2016 macbook pro. All rights reserved.
//

#ifndef PostOrder_Parser_Operations_h
#define PostOrder_Parser_Operations_h

#include <string>
using namespace std;

class Operations
{
public:
    static const char ADDITION_CHAR;
    static const char SUBSTRACTION_CHAR;
    static const char MULTIPLICATION_CHAR;
    static const char DIVISION_CHAR;
    static const char OPPOSITION_CHAR;
    
    static const string DEFAULT_VALUE;
};

#endif
